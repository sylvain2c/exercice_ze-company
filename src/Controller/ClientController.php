<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Client;
use App\Repository\LivreRepository;

use DateTime;

/**
* @Route("/mon_api/api/v1.0")
*/
class ClientController extends AbstractController
{
    /**
     * Permet d'avoir la liste de tous les lecteurs
     * @Route("/lecteur", name="liste_lecteur", methods={"GET"})
     */
    public function listeLecteur()
    {
        $repository   = $this->getDoctrine()->getRepository(Client::class);
        $listeLecteur  = $repository->findAll();
        $listeReponse = array();
        foreach ($listeLecteur as $lecteur) {
	        	$listeReponse[] = array(
        		'id'        => $lecteur->getId(),
        		'nom'       => $lecteur->getNom(),
        		'prenom'    => $lecteur->getPrenom(),
        		'naissance' => date_format ( $lecteur->getDateDeNaissance() , "d/m/Y" ),
        	 );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("lecteur"=>$listeReponse)));
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");
    	return $reponse;
    }

    /**
     * Permet d'avoir les livre d'un lecteur grâce à son id
     * @Route("/lecteur/details/{id}", name="details_lecteur", methods={"GET"})
     */
    public function detailsLecteur($id)
    {
        $repository = $this->getDoctrine()->getRepository(Client::class);
        $lecteur    = $repository->find($id);
        $listeLivre = $lecteur->getLivres();
        $livres = array();
        foreach ($listeLivre as $livre) {
        	$auteur = $livre->getAuteur();
            $livres[] = array(
                "id"     => $livre->getId(),
                "titre"  => $livre->getTitre(),
                "auteur" => $auteur->getPrenom()." ".$auteur->getNom(),
            );
        }
        $reponse = new Response(json_encode(array(
        		'id'             => $lecteur->getId(),
        		'nom'            => $lecteur->getNom(),
        		'prenom'         => $lecteur->getPrenom(),
        		'naissance'      => date_format ( $lecteur->getDateDeNaissance() , "d/m/Y" ),
        		'naissanceModif' => date_format ( $lecteur->getDateDeNaissance() , "Y-m-d" ),
        		'livres'         => $livres,
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");
    	return $reponse;
    }

    /**
     * Permet de créer un lecteur
     * @Route("/lecteur/nouveau/{nom}/{prenom}/{naissance}", name="nouveau_lecteur", methods={"POST"})
     */
    public function nouveauLecteur($nom, $prenom, $naissance)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $lecteur = new Client();
        $lecteur->setNom($nom);
        $lecteur->setPrenom($prenom);
        $lecteur->setDateDeNaissance(new DateTime(date($naissance)));
        $entityManager->persist($lecteur);
		$entityManager->flush();

        $reponse = new Response(json_encode(array(
        		'id'     => $lecteur->getId(),
        		'nom'    => $lecteur->getNom(),
        		'prenom' => $lecteur->getPrenom(),
        		'naissance' => date_format ( $lecteur->getDateDeNaissance() , "d/m/Y" ),
        		)
    		));

    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet de supprimer un lecteur grâce à son id
     * @Route("/lecteur/suppression/{id}", name="suppression_lecteur", methods={"DELETE"})
     */
    public function suppressionLecteur($id)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Client::class);
        $lecteur     = $repository->find($id);
        $entityManager->remove($lecteur);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
        		'nom'    => $lecteur->getNom(),
        		'prenom' => $lecteur->getPrenom(),
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet de modifier le nom et/ou le prenom et/ou date de naissance d'un lecteur grâce à son id 
     * @Route("/lecteur/modification/{id}/{nom}/{prenom}/{naissance}", name="modification_lecteur", methods={"PUT"})
     */
    public function modificationLecteur($id, $nom, $prenom, $naissance)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Client::class);
        $lecteur       = $repository->find($id);
        $lecteur->setNom($nom);
        $lecteur->setPrenom($prenom);
        $lecteur->setDateDeNaissance(new DateTime(date($naissance)));
        $entityManager->persist($lecteur);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
				'id'     => $lecteur->getId(),
        		'nom'    => $lecteur->getNom(),
        		'prenom' => $lecteur->getPrenom()
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet d'ajouter un livre à un lecteur
     * @Route("/lecteur/ajoutLivre/{id}/{idLivre}", name="ajout_livre_lecteur", methods={"PUT"})
     */
    public function ajoutLivreLecteur(LivreRepository $lr, $id, $idLivre)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Client::class);
        $lecteur       = $repository->find($id);
        $livre         = $lr->find(intval($idLivre));
        $lecteur->addLivre($livre);
        $entityManager->persist($lecteur);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
				'id'     => $lecteur->getId(),
        		'nom'    => $lecteur->getNom(),
        		'prenom' => $lecteur->getPrenom()
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet d'ajouter un livre à un lecteur
     * @Route("/lecteur/supprimeLivre/{id}/{idLivre}", name="supprime_livre_lecteur", methods={"PUT"})
     */
    public function supprimeLivreLecteur(LivreRepository $lr, $id, $idLivre)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Client::class);
        $lecteur       = $repository->find($id);
        $livre         = $lr->find($idLivre);
        $lecteur->removeLivre($livre);
        $entityManager->persist($lecteur);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
				'id'     => $lecteur->getId(),
        		'nom'    => $lecteur->getNom(),
        		'prenom' => $lecteur->getPrenom(),
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }
}
