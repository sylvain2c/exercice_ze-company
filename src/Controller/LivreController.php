<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Livre;
use App\Repository\AuteurRepository;
/**
* @Route("/mon_api/api/v1.0")
*/
class LivreController extends AbstractController
{
/**
     * Permet d'avoir la liste de tous les livres
     * @Route("/livre", name="liste_livre", methods={"GET"})
     */
    public function listeLivre()
    {
        $repository   = $this->getDoctrine()->getRepository(Livre::class);
        $listeLivre  = $repository->findAll();
        $listeReponse = array();
        foreach ($listeLivre as $livre) {
        	$auteur = $livre->getAuteur();
        	$listeReponse[] = array(
        		'id'       => $livre->getId(),
        		'titre'    => $livre->getTitre(),
        		'auteur'   => array(
        			"id"     => $auteur->getId(),
        			"nom"    => $auteur->getNom(),
        			"prenom" => $auteur->getPrenom(),
        		),
        	 );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("livre"=>$listeReponse)));
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");
    	return $reponse;
    }

    /**
     * Permet d'avoir les détails sur un livre grâce à son id
     * @Route("/livre/details/{id}", name="details_livre", methods={"GET"})
     */
    public function detailsLivre($id)
    {
        $repository = $this->getDoctrine()->getRepository(Livre::class);
        $livre     = $repository->find($id);
        $auteur = $livre->getAuteur();
        $listeLecteurs = $livre->getLecteurs();
        $lecteurs = [];
        foreach ($listeLecteurs as $lecteur) {
            $lecteurs[] = array(
                "id"        => $lecteur->getId(),
                "nom"       => $lecteur->getNom(),
                "prenom"    => $lecteur->getPrenom(),
                "naissance" => date_format ( $lecteur->getDateDeNaissance() , "d/m/Y" ),
            );
        }
        $reponse    = new Response(json_encode(array(
        		'id'       => $livre->getId(),
        		'titre'    => $livre->getTitre(),
        		'auteur'   => array(
        			"id"     => $auteur->getId(),
        			"nom"    => $auteur->getNom(),
        			"prenom" => $auteur->getPrenom(),
        		),
        		'lecteurs' => $lecteurs,        		
            )
        ));
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");
    	return $reponse;
    }

    /**
     * Permet de créer un livre
     * @Route("/livre/nouveau/{titre}/{idAuteur}", name="nouveau_livre", methods={"POST"})
     */
    public function nouveauLivre(AuteurRepository $ar, $titre, $idAuteur)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $livre = new Livre();
        $livre->setTitre($titre);
        $auteur = $ar->findOneById(intval($idAuteur));
        $livre->setAuteur($auteur);
        $entityManager->persist($livre);
        $entityManager->persist($auteur);
		$entityManager->flush();
        $reponse = new Response(json_encode(array(
        		'id'       => $livre->getId(),
        		'titre'    => $livre->getTitre(),
        		'auteur'   => array(
        			"id"     => $auteur->getId(),
        			"nom"    => $auteur->getNom(),
        			"prenom" => $auteur->getPrenom(),
        		))
    		));

    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet de supprimer un livre grâce à son id
     * @Route("/livre/suppression/{id}", name="suppression_livre", methods={"DELETE"})
     */
    public function suppressionLivre($id)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Livre::class);
        $livre     = $repository->find($id);
        $auteur = $livre->getAuteur();
        $entityManager->remove($livre);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
        		'titre'    => $livre->getTitre(),
        		'auteur'   => array(
        			"id"     => $auteur->getId(),
        			"nom"    => $auteur->getNom(),
        			"prenom" => $auteur->getPrenom(),
        		),
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet de modifier un livre grâce à son id
     * la modification de l'auteur se fait ici
     * la modification des lecteurs se fera via l'entité Client
     * @Route("/livre/modification/{id}/{titre}/{idAuteur}", name="modification_livre", methods={"PUT"})
     */
    public function modificationLivre(AuteurRepository $ar, $id, $titre, $idAuteur)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repoLivre     = $this->getDoctrine()->getRepository(Livre::class);
        $livre         = $repoLivre->find($id);
        // $ancienAuteur  = $ar->find($livre->getAuteur()->getId());
        // $ancienAuteur->removeLivre($livre);
        $livre->setTitre($titre);
        $auteur = $ar->find($idAuteur);
        $livre->setAuteur($auteur);
        $entityManager->persist($livre);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
        		'id'       => $livre->getId(),
        		'titre'    => $livre->getTitre(),
    		))
        );
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }
}