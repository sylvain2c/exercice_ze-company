<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Auteur;

/**
* @Route("/mon_api/api/v1.0")
*/
class AuteurController extends AbstractController
{
	
    /**
     * Permet d'avoir la liste de tous les auteurs
     * @Route("/auteur", name="liste_auteur", methods={"GET"})
     */
    public function listeAuteur()
    {
        $repository   = $this->getDoctrine()->getRepository(Auteur::class);
        $listeAuteur  = $repository->findAll();
        $listeReponse = array();
        foreach ($listeAuteur as $auteur) {
	        	$listeReponse[] = array(
        		'id'     => $auteur->getId(),
        		'nom'    => $auteur->getNom(),
        		'prenom' => $auteur->getPrenom(),
        	 );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("auteur"=>$listeReponse)));
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");
    	return $reponse;
    }

    /**
     * Permet d'avoir les livre d'un auteur grâce à son id
     * @Route("/auteur/details/{id}", name="details_auteur", methods={"GET"})
     */
    public function detailsAuteur($id)
    {
        $repository = $this->getDoctrine()->getRepository(Auteur::class);
        $auteur     = $repository->find($id);
        $listeLivre = $auteur->getLivres();
        $livres = [];
        foreach ($listeLivre as $livre) {
            $livres[] = array(
                "id"    => $livre->getId(),
                "titre" => $livre->getTitre(),
            );
        }
        $reponse = new Response(json_encode(array(
        		'id'     => $auteur->getId(),
        		'nom'    => $auteur->getNom(),
        		'prenom' => $auteur->getPrenom(),
        		'livres' => $livres,
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");
    	return $reponse;
    }

    /**
     * Permet de créer un auteur
     * @Route("/auteur/nouveau/{nom}/{prenom}", name="nouveau_auteur", methods={"POST"})
     */
    public function nouveauAuteur($nom, $prenom)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $auteur = new Auteur();
        $auteur->setNom($nom);
        $auteur->setPrenom($prenom);
        $entityManager->persist($auteur);
		$entityManager->flush();

        $reponse = new Response(json_encode(array(
        		'id'     => $auteur->getId(),
        		'nom'    => $auteur->getNom(),
        		'prenom' => $auteur->getPrenom()
        		)
    		));

    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet de supprimer un auteur grâce à son id
     * @Route("/auteur/suppression/{id}", name="suppression_auteur", methods={"DELETE"})
     */
    public function suppressionAuteur($id)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Auteur::class);
        $auteur     = $repository->find($id);
        $entityManager->remove($auteur);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
        		'nom'    => $auteur->getNom(),
        		'prenom' => $auteur->getPrenom(),
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }

    /**
     * Permet de modifier le nom et/ou le prenom d'un auteur grâce à son id 
     * La gestion des livres de l'auteur se fera via l'entité livre
     * @Route("/auteur/modification/{id}/{nom}/{prenom}", name="modification_auteur", methods={"PUT"})
     */
    public function modificationAuteur($id, $nom, $prenom)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Auteur::class);
        $auteur        = $repository->find($id);
        $auteur->setNom($nom);
        $auteur->setPrenom($prenom);
        $entityManager->persist($auteur);
		$entityManager->flush();
		$reponse = new Response(json_encode(array(
				'id'     => $auteur->getId(),
        		'nom'    => $auteur->getNom(),
        		'prenom' => $auteur->getPrenom(),
        		))
    		);
    	$reponse->headers->set("Content-Type", "application/json");
    	$reponse->headers->set("Access-Control-Allow-Origin", "*");    
    	return $reponse;
    }
}
