<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LivreRepository")
 */
class Livre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Auteur", inversedBy="livres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $auteur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Client", inversedBy="livres")
     */
    private $lecteurs;

    public function __construct()
    {
        $this->lecteurs = new ArrayCollection();
    }

    public function __toString(){
        return $this->titre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAuteur(): ?Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(?Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getLecteurs(): Collection
    {
        return $this->lecteurs;
    }

    public function addLecteur(Client $lecteur): self
    {
        if (!$this->lecteurs->contains($lecteur)) {
            $this->lecteurs[] = $lecteur;
        }

        return $this;
    }

    public function removeLecteur(Client $lecteur): self
    {
        if ($this->lecteurs->contains($lecteur)) {
            $this->lecteurs->removeElement($lecteur);
        }

        return $this;
    }
}
