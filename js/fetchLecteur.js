/* ####### requêtes avec api fetch ####### */
//liste tous les lecteurs de la base
async function tousLecteur(){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur', { method: 'GET' });
	let reponse = await rep.json();
	return reponse['lecteur'];
}

//donne les détails d'un lecteur grâce à son id
async function lecteurById(id){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur/details/'+id, { method: 'GET' });
	let reponse = await rep.json();
	return reponse;
}

//ajoute un lecteur
async function lecteurAjout(nom, prenom, naissance){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur/nouveau/'+nom+'/'+prenom+'/'+naissance, { method: 'POST'});
	let reponse = await rep.json();
	return reponse;
}

//supprime un lecteur grâce à son id
async function lecteurSupprime(id){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur/suppression/'+id, { method: 'DELETE'});
	let reponse = await rep.json();
	return reponse;
}

//modifie un lecteur
async function lecteurModifie(id, nom, prenom, naissance){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur/modification/'+id+'/'+nom+'/'+prenom+'/'+naissance, { method: 'PUT'});
	let reponse = await rep.json();
	return reponse;
}

//ajoute un livre à un lecteur
async function lecteurAjoutLivre(id, idLivre){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur/ajoutLivre/'+id+'/'+idLivre, { method: 'PUT'});
	let reponse = await rep.json();
	return reponse;
}

//supprime un livre à un lecteur
async function lecteurSupprimeLivre(id, idLivre){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/lecteur/supprimeLivre/'+id+'/'+idLivre, { method: 'PUT'});
	let reponse = await rep.json();
	return reponse;
}

/* ##################### */

/* ####### fonctions d'affichage ####### */

//permet l'affichage de tous les lecteurs
async function affichageLecteurs(rep){
	let json = await tousLecteur();
	rep    += '<table class="table table-hover table-striped">';
	rep    +=   '<thead>';
	rep    +=     '<tr>';
	rep    +=        '<th>Prenom Nom</th>';
	rep    +=        '<th>Date de naissance</th>';
	rep    +=        '<th>Livres</th>';
	rep    +=        '<th>Modification</th>';
	rep    +=        '<th>Suppression</th>';
	rep    +=     '</tr>';
	rep    +=   '</thead>';
	rep    +=   '<tbody>';
	json.forEach((elem)=>{
		rep    +=   '<tr>';
		rep    +=     '<td>';
		rep    +=       elem['prenom']+" "+elem['nom'];
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=       elem['naissance'];
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-primary" onclick="affichageLecteurDetails('+elem["id"]+', \' \')"> o- </a>'
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-warning" onclick="affichageLecteurModifie('+elem["id"]+')"> <= </a>';
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-danger" onclick="removeLecteur('+elem["id"]+')"> - </a>'
		rep    +=     '</td>';
		rep    +=   '</tr>';
	});
	rep    +=   '<tr>';
	rep    +=     '<td>';
	rep    +=       '<input type="text" name="prenom" placeholder="Sylvain">';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<input type="text" name="nom" placeholder="Austruy">';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<input type="date" name="naissance">';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<a class="btn btn-success" onclick="ajouteLecteur()"> Ajouter le lecteur </a>';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=     '</td>';
	rep    +=   '</tr>';
	rep    += '</tbody>';
	rep += '</table>';

	$('#affichage').html(rep);
	return true;
}

//permet l'affichage de l'lecteur et de la liste de ses livres
async function affichageLecteurDetails(id, rep){
	let json = await lecteurById(id);
	let livr = await tousLivres();
	rep     += '<h1>Liste des livres du lecteur '+json['prenom']+' '+json['nom']+'</h1>';
	if(json['livres'].length > 0){
		rep     += '<table class="table table-striped table-hover">';
		rep     += '<thead><tr><th>Livres</th><th>Détails</th><th>Supprimer de la liste</th></thead><tbody>';
		json['livres'].forEach((livre)=>{
			rep     +=   '<tr><td>"'+livre['titre']+'" de '+livre["auteur"]+'</td><td><a class="btn btn-primary" onclick="affichageLivreDetails('+livre["id"]+')"> o- </a></td><td><a class="btn btn-danger" onclick="supprimeLivreLecteur('+id+', '+livre["id"]+')"> - </a></td></tr>'
		})
		rep     += '</tbody></table>';
	}
	else{
		rep += '<div class="alert alert-info">Pas encore de livres pour cet lecteur</div>';
	}
	rep     += '<br><h3>Ajouter un livre à la liste de lecture</h3>';
	rep     +=       '<select id="listeLivre">';
	livr.forEach((livre)=>{
		rep    +=       '<option value="'+livre["id"]+'">"'+livre["titre"]+'" de '+livre["auteur"]['prenom']+" "+livre["auteur"]['nom']+'</option>';
	});
	rep     +=       '</select><a class="btn btn-success" onclick="ajouteLivreLecteur('+id+')"> + </a>'
	rep     += '<br><a class="btn btn-primary" onclick="affichageLecteurs(\'\')"> Retour à la liste des lecteurs </a>';
	$('#affichage').html(rep);
	return true;
}

//permet l'affichage du formulaire de modification de l'lecteur
async function affichageLecteurModifie(id){
	let json = await lecteurById(id);
	let rep  = '<h1>Modification du lecteur '+json['prenom']+' '+json['nom']+'</h1>';
	rep     += '<form><label for="nom">Nom : </label><br><input type="text" name="nom" value="'+json["nom"]+'"><br>';
	rep     += '<label for="prenom">Prenom : </label><br><input type="text" name="prenom" value="'+json["prenom"]+'"><br>';
	rep     += '<label for="date">Date : </label><br><input type="date" name="naissance" value="'+json["naissanceModif"]+'"></form>';
	rep     += '<a class="btn btn-success" onclick="modifieLecteur('+id+')"> Enregistrer la modification </a><br>';
	rep     += '<a class="btn btn-primary" onclick="affichageLecteurs(\'\')"> Retour à la liste des lecteurs </a>';
	$('#affichage').html(rep);
	return true;
}

/* ##################### */

/* ####### fonctions de lien entre les requêtes et les fonctions d'affichage ####### */

//permet la suppression d'un lecteur
async function removeLecteur(id){
	if(confirm('Voulez vous vraiment supprimer cet lecteur?')){
		await lecteurSupprime(id);
		affichageLecteurs('<div class="alert alert-warning"><p>lecteur bien supprimé</p></div>');
	}
} 

//permet l'ajout d'un lecteur
async function ajouteLecteur(){
	let nom       = $("input[name='nom']").val();
	let prenom    = $("input[name='prenom']").val();
	let naissance = $("input[name='naissance']").val();
	if(nom != "" && prenom != "" && naissance != ""){
		await lecteurAjout(nom, prenom, naissance);
		affichageLecteurs('<div class="alert alert-success"><p>lecteur bien ajouté</p></div>');
	}
} 

//permet la modification d'un lecteur
async function modifieLecteur(id){
	let nom       = $("input[name='nom']").val();
	let prenom    = $("input[name='prenom']").val();
	let naissance = $("input[name='naissance']").val();
	if(nom != "" && prenom != "" && naissance != ""){
		await lecteurModifie(id, nom, prenom, naissance);
		affichageLecteurs('<div class="alert alert-success"><p>lecteur bien modifié</p></div>');
	}
}

//permet d'ajouter un livre au lecteur
async function ajouteLivreLecteur(id){
	let idLivre = $("#listeLivre option:selected").val();
	await lecteurAjoutLivre(id, idLivre);
	affichageLecteurDetails(id, '<div class="alert alert-success"><p>livre bien ajouté à la liste</p></div>');
}

//permet de supprimer un livre au lecteur
async function supprimeLivreLecteur(id, idLivre){
	await lecteurSupprimeLivre(id, idLivre);
	affichageLecteurDetails(id,'<div class="alert alert-warning"><p>livre bien supprimé de la liste</p></div>');
}