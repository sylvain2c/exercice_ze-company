/* ####### requêtes avec api fetch ####### */
//liste tous les auteurs de la base
async function tousAuteur(){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/auteur', { method: 'GET' });
	let reponse = await rep.json();
	return reponse['auteur'];
}

//donne les détails d'un auteur grâce à son id
async function auteurById(id){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/auteur/details/'+id, { method: 'GET' });
	let reponse = await rep.json();
	return reponse;
}

//ajoute un auteur
async function auteurAjout(nom, prenom){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/auteur/nouveau/'+nom+'/'+prenom, { method: 'POST'});
	let reponse = await rep.json();
	return reponse;
}

//supprime un auteur grâce à son id
async function auteurSupprime(id){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/auteur/suppression/'+id, { method: 'DELETE'});
	let reponse = await rep.json();
	return reponse;
}

//modifie un auteur
async function auteurModifie(id, nom, prenom){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/auteur/modification/'+id+'/'+nom+'/'+prenom, { method: 'PUT'});
	let reponse = await rep.json();
	return reponse;
}

/* ##################### */

/* ####### fonctions d'affichage ####### */

//permet l'affichage de tous les auteurs
async function affichageAuteurs(rep){
	let json = await tousAuteur();

	rep    += '<table class="table table-hover table-striped">';
	rep    +=   '<thead>';
	rep    +=     '<tr>';
	rep    +=        '<th>Prenom</th>';
	rep    +=        '<th>Nom</th>';
	rep    +=        '<th>Livres</th>';
	rep    +=        '<th>Modification</th>';
	rep    +=        '<th>Suppression</th>';
	rep    +=     '</tr>';
	rep    +=   '</thead>';
	rep    +=   '<tbody>';
	json.forEach((elem)=>{
		rep    +=   '<tr>';
		rep    +=     '<td>';
		rep    +=       elem['prenom'];
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=       elem['nom'];
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-primary" onclick="affichageAuteurDetails('+elem["id"]+')"> o- </a>'
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-warning" onclick="affichageAuteurModifie('+elem["id"]+')"> <= </a>';
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-danger" onclick="removeAuteur('+elem["id"]+')"> - </a>'
		rep    +=     '</td>';
		rep    +=   '</tr>';
	});
	rep    +=   '<tr>';
	rep    +=     '<td>';
	rep    +=       '<input type="text" name="prenom" placeholder="Boris">';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<input type="text" name="nom" placeholder="Vian">';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<a class="btn btn-success" onclick="ajouteAuteur()"> Ajouter l\'auteur </a>';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=     '</td>';
	rep    +=   '</tr>';
	rep    += '</tbody>';
	rep += '</table>';

	$('#affichage').html(rep);
	return true;
}

//permet l'affichage de l'auteur et de la liste de ses livres
async function affichageAuteurDetails(id){
	let json = await auteurById(id);
	let rep  = '<h1>Liste des livres de l\'auteur '+json['prenom']+' '+json['nom']+'</h1>';
	if(json['livres'].length > 0){
		rep     += '<table class="table table-striped table-hover">';
		rep     += '<thead><tr><th>Livre</th><th>Détails</th></thead><tbody>';
		json['livres'].forEach((livre)=>{
			rep     +=   '<tr><td>"'+livre['titre']+'"</td><td><a class="btn btn-primary" onclick="affichageLivreDetails('+livre["id"]+')"> o- </a></td></tr>'
		})
		rep     += '</tbody></table>';
	}
	else{
		rep += '<div class="alert alert-info">Pas encore de livres pour cet auteur</div>';
	}
	
	rep     += '<a class="btn btn-primary" onclick="affichageAuteurs(\'\')"> Retour à la liste des auteurs </a>';
	$('#affichage').html(rep);
	return true;
}

//permet l'affichage du formulaire de modification de l'auteur
async function affichageAuteurModifie(id){
	let json = await auteurById(id);
	let rep  = '<h1>Modification de l\'auteur '+json['prenom']+' '+json['nom']+'</h1>';
	rep     += '<form><label for="nom">Nom : </label><br><input type="text" name="nom" value="'+json["nom"]+'"><br>';
	rep     += '<label for="prenom">Prenom : </label><br><input type="text" name="prenom" value="'+json["prenom"]+'"></form>';
	rep     += '<br><a class="btn btn-success" onclick="modifieAuteur('+id+')"> Enregistrer la modification </a><br>';
	rep     += '<a class="btn btn-primary" onclick="affichageAuteurs(\'\')"> Retour à la liste des auteurs </a>';
	$('#affichage').html(rep);
	return true;
}

/* ##################### */

/* ####### fonctions de lien entre les requêtes et les fonctions d'affichage ####### */

//permet la suppression d'un auteur
async function removeAuteur(id){
	if(confirm('Voulez vous vraiment supprimer cet auteur?')){
		await auteurSupprime(id);
		affichageAuteurs('<div class="alert alert-warning"><p>Auteur bien supprimé</p></div>');
	}
} 

//permet l'ajout d'un auteur
async function ajouteAuteur(){
	let nom    = $("input[name='nom']").val();
	let prenom = $("input[name='prenom']").val();
	if(nom != "" && prenom != ""){
		await auteurAjout(nom, prenom);
		affichageAuteurs('<div class="alert alert-success"><p>Auteur bien ajouté</p></div>');
	}
} 

//permet la modification d'un auteur
async function modifieAuteur(id){
	let nom    = $("input[name='nom']").val();
	let prenom = $("input[name='prenom']").val();
	if(nom != "" && prenom != ""){
		await auteurModifie(id, nom, prenom);
		affichageAuteurs('<div class="alert alert-success"><p>Auteur bien modifié</p></div>');
	}
}