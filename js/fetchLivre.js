/* ####### requêtes avec api fetch ####### */
//liste tous les livres de la base
async function tousLivres(){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/livre', { method: 'GET' });
	let reponse = await rep.json();
	return reponse['livre'];
}

//donne les détails d'un livre grâce à son id
async function livreById(id){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/livre/details/'+id, { method: 'GET' });
	let reponse = await rep.json();
	return reponse;
}

//ajoute un livre
async function livreAjout(titre, auteur){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/livre/nouveau/'+titre+'/'+auteur, { method: 'POST'});
	let reponse = await rep.json();
	return reponse;
}

//supprime un livre grâce à son id
async function livreSupprime(id){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/livre/suppression/'+id, { method: 'DELETE'});
	let reponse = await rep.json();
	return reponse;
}

//modifie un livre
async function livreModifie(id, titre, auteur){
	let rep = await fetch('http://127.0.0.1:8000/mon_api/api/v1.0/livre/modification/'+id+'/'+titre+'/'+auteur, { method: 'PUT'});
	let reponse = await rep.json();
	return reponse;
}

/* ##################### */

/* ####### fonctions d'affichage ####### */

//permet l'affichage de tous les livres
async function affichageLivres(rep){
	let json    = await tousLivres();
	let auteurs = await tousAuteur();
	rep    += '<table class="table table-hover table-striped">';
	rep    +=   '<thead>';
	rep    +=     '<tr>';
	rep    +=        '<th>Titre</th>';
	rep    +=        '<th>Auteur</th>';
	rep    +=        '<th>Lecteurs</th>';
	rep    +=        '<th>Modification</th>';
	rep    +=        '<th>Suppression</th>';
	rep    +=     '</tr>';
	rep    +=   '</thead>';
	rep    +=   '<tbody>';
	json.forEach((elem)=>{
		rep    +=   '<tr>';
		rep    +=     '<td>';
		rep    +=       elem['titre'];
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=       elem['auteur']['prenom']+" "+elem['auteur']['nom'];
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-primary" onclick="affichageLivreDetails('+elem["id"]+')"> o- </a>'
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-warning" onclick="affichageLivreModifie('+elem["id"]+')"> <= </a>';
		rep    +=     '</td>';
		rep    +=     '<td>';
		rep    +=     '<a class="btn btn-danger" onclick="removeLivre('+elem["id"]+')"> - </a>'
		rep    +=     '</td>';
		rep    +=   '</tr>';
	});
	rep    +=   '<tr>';
	rep    +=     '<td>';
	rep    +=       '<input type="text" name="titre" placeholder="L\'Écume des jours">';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<select id="listeAuteur">';
	auteurs.forEach((auteur)=>{
		rep    +=       '<option value="'+auteur["id"]+'">'+auteur["prenom"]+" "+auteur["nom"]+'</option>';
	});
	rep    +=       '</select>'
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=       '<a class="btn btn-success" onclick="ajouteLivre()"> Ajouter le livre </a>';
	rep    +=     '</td>';
	rep    +=     '<td>';
	rep    +=     '</td>';
	rep    +=   '</tr>';
	rep    += '</tbody>';
	rep += '</table>';
	$('#affichage').html(rep);
	return true;
}

//permet l'affichage dulivre et de la liste de ses livres
async function affichageLivreDetails(id){
	let json = await livreById(id);
	let rep  = '<h1>Liste des lecteurs du livre "'+json['titre']+'" de '+json['auteur']['prenom']+" "+json['auteur']['nom']+'</h1>';
	if(json['lecteurs'].length > 0){
		rep     += '<table class="table table-striped table-hover">';
		rep     += '<thead><tr><th>Lecteurs</th><th>Détails</th></thead><tbody>';
		json['lecteurs'].forEach((lecteur)=>{
			rep     +=   '<tr><td>'+lecteur['prenom']+" "+lecteur['prenom']+'</td><td><a class="btn btn-primary" onclick="affichageLecteurDetails('+lecteur["id"]+')"> o- </a></td></tr>'
		})
		rep     += '</tbody></table>';
	}
	else{
		rep += '<div class="alert alert-info">Pas encore de lecteurs pour ce livre</div>';
	}
	
	rep     += '<a class="btn btn-primary" onclick="affichageLivres(\'\')"> Retour à la liste des livres </a>';
	$('#affichage').html(rep);
	return true;
}

//permet l'affichage du formulaire de modification de l'livre
async function affichageLivreModifie(id){
	let json    = await livreById(id);
	let auteurs = await tousAuteur();
	let rep  = '<h1>Modification du livre '+json['titre']+'</h1>';
	rep     += '<form><label for="titre">Titre : </label><br><input type="text" name="titre" value="'+json["titre"]+'"><br><br>';
	rep     += '<label for="listeAuteur">Auteur : </label><select id="changeAuteur">';
	auteurs.forEach((auteur)=>{
		rep    +=       '<option value="'+auteur["id"]+'">'+auteur["prenom"]+" "+auteur["nom"]+'</option>';
	});
	rep     += '</select></form>';
	rep     += '<a class="btn btn-success" onclick="modifieLivre('+id+')"> Enregistrer la modification </a><br>';
	rep     += '<a class="btn btn-primary" onclick="affichageLivres(\'\')"> Retour à la liste des livres </a>';
	$('#affichage').html(rep);
	return true;
}

/* ##################### */

/* ####### fonctions de lien entre les requêtes et les fonctions d'affichage ####### */

//permet la suppression d'un livre
async function removeLivre(id){
	if(confirm('Voulez vous vraiment supprimer cet livre?')){
		await livreSupprime(id);
		affichageLivres('<div class="alert alert-warning"><p>livre bien supprimé</p></div>');
	}
} 

//permet l'ajout d'un livre
async function ajouteLivre(){
	let titre  = $("input[name='titre']").val();
	let auteur = $("#listeAuteur option:selected").val();
	if(titre != ""){
		await livreAjout(titre, auteur);
		affichageLivres('<div class="alert alert-success"><p>livre bien ajouté</p></div>');
	}
} 

//permet la modification d'un livre
async function modifieLivre(id){
	let titre  = $("input[name='titre']").val();
	let auteur = $("#changeAuteur option:selected").val();
	if(titre != ""){
		await livreModifie(id, titre, auteur);
		affichageLivres('<div class="alert alert-success"><p>livre bien modifié</p></div>');
	}
}