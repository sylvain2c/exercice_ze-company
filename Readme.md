

# Exercice PHP/JS --Rendu--

## Introduction
Projet sous Symfony 4.

## Commandes
### Pré-requis

 - Avoir **Composer** (<https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx>)
 - Avoir **NodeJS** (<https://nodejs.org/en/download/package-manager/>)

### Initialisation du projet
Commencez par créer une base de données et modifier le fichier .env.
```
DATABASE_URL=mysql://<user>:<password>@127.0.0.1:3306/<db-name>
```

Puis, il faut exécuter les commandes suivantes.
```bash
composer install
php bin/console doctrine:migrations:migrate
npm install.
```

### Compiler
```bash
npm run dev
```

## Utilisation
Afin de pouvoir lancer l'application il faut démarrer un premier server pour l'API, puis un second afin de pouvoir accéder au service API via l'interface en HTML/JS.
```bash
php bin/console server:start 127.0.0.1:8000 #API
php -S localhost:8001 #interface
```
Il ne reste plus qu'à utiliser l'interface pour intéragir avec l'API : http://localhost:8001/interfaceApi.html

### Créer un livre
Il faut dans un premier temps créer un auteur (si l'auteur du livre n'est pas déjà dans la base de données), sinon la création du livre ne pourra pas se faire (pas de livre sans auteur). Ensuite le livre peut être créé.

## L'exercice
L'exercice rendu est une single-page application créée en HTML/JS, utilisant la bibliothèque jQuery et pour la mise en page la librairie Bootstrap. Utilisation de la syntaxe async/await afin de faciliter l'écriture, la lecture et la maintenabilité du code.

L'entité livre et les controllers ont été générés avec le MakerBundle.
Création des fichier :
- ./interfaceApi.html (contient le code de la page web ainsi que les imports utiles à l'application)
- ./js (dossier contenant les codes JavaScript pour fetcher l'API)
- ./js/fetchAuteur.js (code pour gérer l'entité Auteur)
- ./js/fetchLecteur.js (code pour gérer l'entité Client)
- ./js/fetchLivre.js (code pour gérer l'entité Livre)

## Liens utiles

 - **Webpack Encore (Symfony)** : <https://symfony.com/doc/current/frontend.html> (pour les fichiers .js et .scss)
 - **MakerBundle (Symfony)** : <https://symfony.com/doc/current/bundles/SymfonyMakerBundle/index.html> (pour générer les entités et les controlleurs)
 - **API Fetch (JavaScript)** : <https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch> (pour les requêtes à l'API Rest)
